package com.calculator.client;

import com.calculator.client.commandLine.CommandLineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableConfigurationProperties
public class SpringBootConsoleApplication
    implements CommandLineRunner {

  private static Logger LOG = LoggerFactory
      .getLogger(SpringBootConsoleApplication.class);

  @Autowired
  private CommandLineService commandLineService;

  public static void main(String[] args) {
    LOG.info("STARTING THE APPLICATION");
    SpringApplication.run(SpringBootConsoleApplication.class, args);
    LOG.info("APPLICATION FINISHED");
  }

  @Override
  public void run(String... args) {
    commandLineService.run();
  }

  @Bean
  public RestTemplate getRestTemplate() {
    return new RestTemplate();
  }
}