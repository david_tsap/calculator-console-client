package com.calculator.client.model;

import com.calculator.client.constant.Operation;


public class Expression {

  float number1;
  float number2;
  Operation operation;
  float result;

  public float getNumber1() {
    return number1;
  }

  public void setNumber1(float number1) {
    this.number1 = number1;
  }

  public float getNumber2() {
    return number2;
  }

  public void setNumber2(float number2) {
    this.number2 = number2;
  }

  public Operation getOperation() {
    return operation;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public float getResult() {
    return result;
  }

  public void setResult(float result) {
    this.result = result;
  }

  @Override
  public String toString() {
    return "Expression{" +
        "number1=" + number1 +
        ", number2=" + number2 +
        ", operation=" + operation +
        ", result=" + result +
        '}';
  }
}
