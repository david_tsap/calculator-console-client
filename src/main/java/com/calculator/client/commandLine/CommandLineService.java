package com.calculator.client.commandLine;

import com.calculator.client.Error.CalculatorClientException;
import com.calculator.client.client.CalculatorServerClient;
import com.calculator.client.constant.Errors;
import com.calculator.client.model.Expression;
import java.io.InputStreamReader;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/***
 * starting point from user interface.
 * the program will run until the user will write quit.
 * after writing the expression to calculate the user should press "enter"
 */
@Service
public class CommandLineService implements UserInterfaceService {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private boolean endOfProgram = true;

  @Autowired private CommandLineParserService commandLineParserService;

  @Autowired CalculatorServerClient calculatorServerClient;

  @Override
  public void run() {
    Scanner scanner = new Scanner(new InputStreamReader(System.in));
    System.out.println("Welcome to console calculator application");
    while (endOfProgram) {
      System.out.println("please enter your mathematical calculations, To quite write quit");
      String calc = scanner.nextLine();
      if (!calc.equals("quit")) {
        System.out.println("got the following calculation:" + calc);
        handelInput(calc);
      } else {
        endOfProgram = false;
      }
    }
  }

  private void handelInput(String calc) {
    try {
      Expression expression = commandLineParserService.parse(calc);
      if (expression != null) {
        Expression response = calculatorServerClient.sendCalculationRequest(expression);
        System.out.println(
            "Result : "
                + response.getNumber1()
                + response.getOperation().getValue()
                + response.getNumber2()
                + " = "
                + response.getResult());
      } else {
        System.out.println("Input format is wrong.");
        System.out.println("The string format should be \"number\" \"operation\" \"number\"");
        System.out.println("for example 2+2 , 3*6 , 4/2 , 10-5");
      }
    } catch (CalculatorClientException exception) {
      if (exception.getErrorCode().equals(Errors.CALC_10001)) {
        System.out.println("divide by zero not allowed");
      } else if (exception.getErrorCode().equals(Errors.CALC_10000)) {
        System.out.println("operation not supported");
      } else {
        System.out.println("internal error");
      }
    } catch (Exception ex) {
      logger.debug(ex.getMessage());
      System.out.println("internal error");
    }
  }
}
