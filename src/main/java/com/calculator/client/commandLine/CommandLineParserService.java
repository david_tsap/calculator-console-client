package com.calculator.client.commandLine;

import com.calculator.client.constant.Operation;
import com.calculator.client.model.Expression;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/***
 * command line parser will parse user input(String) to Expression object
 * The string format should be "number" "operation" "number"
 * for example:
 * 2+2
 * 3 * 4
 * space is allowed
 * operation should be one of : "+" , "-" , "*" , "/"
 *
 */
@Service
public class CommandLineParserService {



  public Expression parse(String calc) {
    String trimCalc = calc.replaceAll(" ","");
    // check if the format is number , one of +,-,/,* ,  number
    Pattern pattern = Pattern.compile("^\\d+(\\+|-|\\*|/)\\d+$");
    Matcher matcher = pattern.matcher(trimCalc);
    if (matcher.find()) {
      String group = matcher.group(1);
      String[] split = trimCalc.split("\\+|-|\\*|/");
      if (split.length == 2) {
        Expression expression = new Expression();
        expression.setNumber1(Integer.parseInt(split[0]));
        expression.setNumber2(Integer.parseInt(split[1]));
        expression.setOperation(Operation.fromString(group));
        return expression;
      }
    }
    return null;
  }
}
