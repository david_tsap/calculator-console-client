package com.calculator.client.client;

import com.calculator.client.model.Expression;

/***
 * interface client to send requests to server
 */
public interface CalculatorServerClient {

   Expression sendCalculationRequest(Expression expression);
}
