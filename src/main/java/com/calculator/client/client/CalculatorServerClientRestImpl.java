package com.calculator.client.client;

import com.calculator.client.Error.CalculatorClientException;
import com.calculator.client.constant.Errors;
import com.calculator.client.model.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
/**
 * client for sending request to server
 * Alternative, can be implement with GRPC protocol
 * */

@Service
public class CalculatorServerClientRestImpl implements CalculatorServerClient{

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired
  protected RestTemplate restTemplate;

  private String CALC_URL =  "/api/calc";

  private String fullURL;

  public CalculatorServerClientRestImpl(
      @Value("${endpoint.calculator.host}")String host,
      @Value("${endpoint.calculator.port}")String port
  ){
    fullURL = host + ":" + port + CALC_URL;
  }
  @Override
  public Expression sendCalculationRequest(Expression expression) {
    logger.debug("sending rest request to :" + fullURL );
    UriComponents url = UriComponentsBuilder
        .fromHttpUrl(fullURL).buildAndExpand();
    try{
    ResponseEntity<Expression> exchange = restTemplate.exchange(
        new RequestEntity<>(expression,createHttpHeaders(), HttpMethod.POST, url.toUri()),
        new ParameterizedTypeReference<Expression>() {
        });
    Expression body = exchange.getBody();
    logger.debug("received response from :" + exchange );
    return body;
    }
    catch (HttpStatusCodeException exception){
      throw new CalculatorClientException(exception.getResponseBodyAsString()).setErrorCode(Errors.fromString(exception.getResponseBodyAsString()));
    }
  }

  private HttpHeaders createHttpHeaders(){
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return httpHeaders;
  }
}
