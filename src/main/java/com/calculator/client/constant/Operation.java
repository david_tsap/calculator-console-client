package com.calculator.client.constant;

public enum Operation {
  ADDITION("+"),
  SUBTRACTION("-"),
  MULTIPLICATION("*"),
  DIVISION("/");

  private String value;

   Operation(String value) {
    this.value = value;
  }

  public static Operation fromString(String text) {
    for (Operation operation : Operation.values()) {
      if (operation.value.equalsIgnoreCase(text)) {
        return operation;
      }
    }
    return null;
  }

  public String getValue() {
    return value;
  }
}
