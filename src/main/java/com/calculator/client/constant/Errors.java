package com.calculator.client.constant;

/***
 * User errors
 */
public enum Errors {
  CALC_10000, //operation not supported
  CALC_10001; //divide by zero not allowed

  public static Errors fromString(String text) {
    for (Errors errors : Errors.values()) {
      if (errors.toString().equalsIgnoreCase(text)) {
        return errors;
      }
    }
    return null;
  }
}
