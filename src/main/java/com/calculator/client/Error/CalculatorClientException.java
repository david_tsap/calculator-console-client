package com.calculator.client.Error;

import com.calculator.client.constant.Errors;

public class CalculatorClientException extends RuntimeException{

  protected Errors errorCode;

  public CalculatorClientException(String massage) {
    super(massage);
  }

  public CalculatorClientException setErrorCode(Errors errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  public Errors getErrorCode() {
    return errorCode;
  }

}
