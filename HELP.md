### General
calculator client is a console client that can accept simple mathematical calculations, send expression
to calculator server and print the result.
The client support 4 binary operations: 
*	addition (+)
*	subtraction (-)
*	multiplication (*)
*	division (/)  
Input format should be "number" "operation" "number"
 * spaces allowed
 * operation should be one of : "+" , "-" , "*" , "/"
 * divide by zero not allowed  
for example : 
 * 2+2
 * 3 * 4
 * 6-2
 * 4/2

change service port in application.properties file.  
change server configuration in application.properties file.

### Build
In root directory run:  
mvn clean install

### Run
After running compile using maven in target directory "accessControlService-0.0.1-SNAPSHOT.jar" will
be created.
go to target dir and run:  
java -jar calculator-client-1.0-SNAPSHOT.jar

###Setup

1. clone git repo
2. cd to root directory
3. run mvn clean install
4. cd target folder
5. run java -jar java -jar calculator-client-1.0-SNAPSHOT.jar